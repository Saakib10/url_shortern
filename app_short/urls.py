from django.urls import path

# Import the home view
from .views import home_view, load_url_list, redirect_url_view

appname = "app_short"

urlpatterns = [
    # Home view
    path("", home_view, name="home"),
    path('<str:shortened_part>', redirect_url_view, name='redirect'),
    path("url_list/", load_url_list, name="url_list"),
]